import React from 'react'
import Layout from '~/components/Layout'
import styles from '~/styles/Home.module.css'

const About = () => {
  return (
    <Layout>
      <div className={styles.grid}>
        <code style={{fontSize: 28}}>
          I'm a freelancer engineer with over 13 years of
          experience. I have worked in many industries: e-learning, online ticketing, marketing, e-commerce, etc. I help companies align their technological solutions to their business objectives.
        </code>
      </div>
    </Layout>
  )
}

export default About