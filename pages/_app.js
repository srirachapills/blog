import React from 'react'
import '~/styles/globals.css'
import '~/components/Mixpanel'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
