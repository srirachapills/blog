import React from 'react'
import Link from 'next/link'
import styles from '~/styles/Home.module.css'
import Layout from '~/components/Layout'

export default function Home() {
  return (
    <Layout>
      <div className={styles.grid}>

        <Link href="/posts">
          <a className={styles.card}>
            <h3>Stories &rarr;</h3>
            <p>Software development thoughts</p>
          </a>
        </Link>

        <Link href="/resume">
          <a className={styles.card}>
            <h3>Hire me &rarr;</h3>
            <p>My resume and ambitions</p>
          </a>
        </Link>

        <Link href="/about">
          <a className={styles.card}>
            <h3>About &rarr;</h3>
            <p>More about myself</p>
          </a>
        </Link>

        <Link href="/social">
          <a className={styles.card}>
            <h3>Social &rarr;</h3>
            <p>
              Social media presence
              </p>
          </a>
        </Link>

      </div>
    </Layout>
  )
}
