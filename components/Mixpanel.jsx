import React from 'react'
import mixpanel from 'mixpanel-browser';

let env_check = process.env.NODE_ENV === 'production';

if (process.env.MIXPANEL_API_KEY && env_check) {
  mixpanel.init(process.env.MIXPANEL_API_KEY);
}

let actions = {
  identify: (id) => {
    if (env_check) mixpanel.identify(id);
  },
  alias: (id) => {
    if (env_check) mixpanel.alias(id);
  },
  track: (name, props) => {
    if (env_check) mixpanel.track(name, props);
  },
  people: {
    set: (props) => {
      if (env_check) mixpanel.people.set(props);
    },
  },
};

export let Mixpanel = actions;