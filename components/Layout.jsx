import React from 'react'
import styles from '~/styles/Home.module.css'
import Head from 'next/head'
import Link from 'next/link'

const Layout = ({ children }) => {
  return (
    <>
      <Head>
        <title>JP Melanson</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.container}>

        <div style={
          {
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'space-between',
            minHeight: '6em',
          }
        }>
          <Link href="/about">
            <img src="/jp.jpg" className={styles.avatar}/>
          </Link>
          <Link href="/">
            <a>
              <h1 className={styles.title}>
                the nordic developer
              </h1>
            </a>
          </Link>
          <h1 className={styles.title}>🥶</h1>
        </div>

        <main className={styles.main}>
          {children}
        </main>

        <footer className={styles.footer}>
          <a
            target="_blank"
            rel="noopener noreferrer"
          >
            Powered by{' '}
            <img src="/next.png" alt="Next.js Logo" className={styles.logo} />
          </a>
        </footer>
      </div>
    </>
  );
}

export default Layout;